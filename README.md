# CVL Python HDF5

This repo contains python utilities for simplifying work with hdf5 files.

Package works correctly on python >= 3.6.

Full documentation can be found here: https://cvisionlab.gitlab.io/cvl-python-hdf5/

## Installation
Use `pip install git+https://gitlab.com/CVisionLab/cvl-python-hdf5.git` to install latest
version or `pip install git+https://gitlab.com/CVisionLab/cvl-python-hdf5.git@<version>`
to install specific version.

## Compatibility policy (supported python versions)
This repository follows [official status of python branches](https://devguide.python.org/#status-of-python-branches).

This means:
- We aim to add support for new versions as they appear in the list.
- We drop support for outdated versions when they are removed from the list.

