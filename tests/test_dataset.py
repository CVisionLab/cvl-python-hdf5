"""Dataset tests."""

import os
import h5py
import numpy as np

import pytest

from cvl import hdf5


def test_dataset_creation(tmpdir):
    """Test dataset file created on disk."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        attrs={'attr': 25},
        data=hdf5.DatasetDescription((10, 10), 'float32',
                                     attrs={'attr': 19})
    )
    dsfile.close()
    assert(os.path.isfile(dsfile_name))

    dsfile = h5py.File(dsfile_name, 'r')

    assert(list(dsfile.keys()) == ['data'])
    assert(dsfile['data'].shape == (0, 10, 10))
    assert(dsfile.attrs['attr'] == 25)
    assert (dsfile['data'].attrs['attr'] == 19)

    dsfile.close()


def test_dataset_writing(tmpdir):
    """Test dataset content."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        data=hdf5.DatasetDescription((10, 10), 'uint8')
    )

    hdf5.store_data(dsfile, data=[
        np.ones((10, 10), dtype='uint8') * 1,
        np.ones((10, 10), dtype='uint8') * 2,
        np.ones((10, 10), dtype='uint8') * 3,
    ])
    hdf5.store_data(dsfile, data=[
        np.ones((10, 10), dtype='uint8') * 4,
        np.ones((10, 10), dtype='uint8') * 5,
        np.ones((10, 10), dtype='uint8') * 6,
    ])
    hdf5.store_data(dsfile, data=[
        np.ones((10, 10), dtype='uint8') * 7,
        np.ones((10, 10), dtype='uint8') * 8,
    ])
    # Single elem add
    hdf5.store_data(dsfile, data=np.ones((10, 10), dtype='uint8') * 9)

    dsfile.close()

    dsfile = h5py.File(dsfile_name, 'r')

    test_data = np.arange(1, 10, dtype='uint8')[..., None, None]
    test_data = test_data.repeat(10, axis=1).repeat(10, axis=2)

    assert(np.alltrue(test_data == dsfile['data']))
    dsfile.close()


def test_dataset_chunksize(tmpdir):
    """Check dataset chunking."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        attrs={'attr': 25},
        data=hdf5.DatasetDescription((10, 10), 'float32', chunksize=2)
    )
    dsfile.close()
    assert(os.path.isfile(dsfile_name))

    dsfile = h5py.File(dsfile_name, 'r')

    assert(dsfile['data'].chunks == (2, 10, 10))


def test_compression(tmpdir):
    """Check dataset compression set."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        attrs={'attr': 25},
        data1=hdf5.DatasetDescription((10, 10), 'float32', compression='gzip'),

        data2=hdf5.DatasetDescription((10, 10), 'float32',
                                      compression='gzip', chunksize=5),

        data3=hdf5.DatasetDescription((10, 10), 'float32',
                                      compression='gzip', compression_opts=9,
                                      chunksize=5)
    )
    dsfile.close()
    assert (os.path.isfile(dsfile_name))

    dsfile = h5py.File(dsfile_name, 'r')

    assert dsfile['data1'].compression == 'gzip'
    assert dsfile['data1'].compression_opts == 4
    assert dsfile['data2'].compression == 'gzip'
    assert dsfile['data2'].compression_opts == 4
    assert dsfile['data3'].compression == 'gzip'
    assert dsfile['data3'].compression_opts == 9


def test_packds_compression(tmpdir):
    """Check Packed dataset compression set."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        attrs={'attr': 25},

        data=hdf5.DatasetDescription((10, 10), 'float32',
                                     pack_axis=1,
                                     compression='gzip', compression_opts=9,
                                     chunksize=5)
    )
    dsfile.close()
    assert (os.path.isfile(dsfile_name))

    dsfile = h5py.File(dsfile_name, 'r')

    assert dsfile['data'].compression == 'gzip'
    assert dsfile['data'].compression_opts == 9


def test_dataset_description_type(tmpdir):
    """Test dataset description type is enforced."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    with pytest.raises(TypeError):
        hdf5.init_storage(dsfile_name, data=((10, 10), 'float32'))


def test_dataset_wrong_dsname(tmpdir):
    """Test writing attempt with wrong name."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        data=hdf5.DatasetDescription((10, 10), 'uint8')
    )

    with pytest.raises(KeyError):
        hdf5.store_data(dsfile, wrong_data=[
            np.ones((10, 10), dtype='uint8') * 1,
            np.ones((10, 10), dtype='uint8') * 2,
            np.ones((10, 10), dtype='uint8') * 3,
        ])

    dsfile.close()
