"""HDF5 wrapper for tf.Dataset tests."""

import os
import h5py
import numpy as np

import pytest
tensorflow = pytest.importorskip('tensorflow', minversion='2.0')

import cvl.hdf5 as hdf5
import cvl.hdf5.tf


@pytest.fixture
def hdf5dataset(tmpdir):
    """Create h5-dataset for tests and return all necessary info."""
    keys = [
        'uint8_d', 'str_d', 'float32_d'
    ]
    descrs = [
        hdf5.DatasetDescription(tuple(), 'uint8'),
        hdf5.DatasetDescription(tuple(), h5py.special_dtype(vlen=str)),
        hdf5.DatasetDescription((3, 2), 'float32')
    ]
    data = [
        np.arange(1, 10, dtype='uint8'),
        np.array(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'z']),
        np.random.uniform(0, 1, (9, 3, 2)).astype('float32')
    ]

    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name, **dict(zip(keys, descrs)))
    hdf5.store_data(dsfile, **dict(zip(keys, data)))
    dsfile.attrs['size'] = 9
    dsfile.close()

    return {
        'path': dsfile_name,
        'keys': keys,
        'data': data,
    }


def test_load(hdf5dataset):
    """Test load correctness."""
    ds_info = hdf5dataset
    dtypes = [el.dtype for el in ds_info['data']]

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ds_info['keys'], dtypes, shuffle=False)

    count = 0
    ds_iter = iter(h5_wrapper.dataset)
    while count < len(ds_info['keys']):
        batch = next(ds_iter)
        for el_i, el in enumerate(batch):
            read_el = el.numpy()[0]
            true_el = ds_info['data'][el_i][count]
            if isinstance(true_el, str):
                assert isinstance(read_el, bytes)
                read_el = read_el.decode()
            assert np.alltrue(read_el == true_el)
        count += 1


def test_load_shuffled(hdf5dataset):
    """Test shuffled load correctness."""
    ds_info = hdf5dataset
    dtypes = [el.dtype for el in ds_info['data']]

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ds_info['keys'], dtypes, shuffle=True)

    is_shuffled = False
    data_marks = [np.zeros(d.shape[0],).astype('bool')
                  for d in ds_info['data']]
    #
    count = 0
    ds_iter = iter(h5_wrapper.dataset)
    while count < len(ds_info['data'][0]):
        batch = next(ds_iter)
        for el_i, el in enumerate(batch):
            read_el = el.numpy()[0]
            pos_el = ds_info['data'][el_i][count]
            # Check shuffled property
            if isinstance(pos_el, str):
                assert isinstance(read_el, bytes)
                read_el = read_el.decode()
            is_shuffled = np.any(read_el != pos_el) | is_shuffled
            # Mark loaded data
            for data_elem_i, data_elem in enumerate(ds_info['data'][el_i]):
                if np.alltrue(data_elem == read_el):
                    data_marks[el_i][data_elem_i] = True
        count += 1

    # Read data must be shuffled and coverage all data
    assert np.all(data_marks)
    assert is_shuffled


def test_outiside_graph_f(hdf5dataset):
    """Test outside graph function correctness."""
    ds_info = hdf5dataset

    def out_f(uint_data, _, float_data):
        new_float_data = (uint_data / 255.0).astype('float32')
        new_uint_data = (float_data * 255).astype('uint8')
        return new_float_data, new_uint_data

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ds_info['keys'], ['float32', 'uint8'],
        shuffle=False, outside_graph_fn=out_f)

    #
    count = 0
    ds_iter = iter(h5_wrapper.dataset)
    while count < len(ds_info['data'][0]):
        read_float_el, read_uint_el = next(ds_iter)
        true_float_el = ds_info['data'][0][count] / 255.0
        true_uint_el = (ds_info['data'][2][count] * 255).astype('uint8')
        assert np.abs(read_float_el - true_float_el) < 1e-3
        assert np.alltrue(read_uint_el == true_uint_el)
        count += 1


def test_inside_graph_f(hdf5dataset):
    """Test inside graph function correctness."""
    ds_info = hdf5dataset

    def in_f(uint_data, float_data):
        return uint_data + 5, float_data + 0.75

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ['uint8_d', 'float32_d'], ['uint8', 'float32'],
        shuffle=False, inside_graph_fn=in_f)

    #
    count = 0
    ds_iter = iter(h5_wrapper.dataset)
    while count < len(ds_info['data'][0]):
        read_uint_el, read_float_el = next(ds_iter)
        true_uint_el = ds_info['data'][0][count] + 5
        true_float_el = ds_info['data'][2][count] + 0.75
        assert np.alltrue(np.abs(read_float_el - true_float_el) < 1e-3)
        assert np.alltrue(read_uint_el == true_uint_el)
        count += 1


def test_nthreads(hdf5dataset):
    """Test shuffled load correctness."""
    ds_info = hdf5dataset
    dtypes = [el.dtype for el in ds_info['data']]

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ds_info['keys'], dtypes, shuffle=True)

    data_marks = [np.zeros(d.shape[0],).astype('bool')
                  for d in ds_info['data']]
    #
    count = 0
    ds_iter = iter(h5_wrapper.dataset)
    while count < len(ds_info['data'][0]):
        batch = next(ds_iter)
        for el_i, el in enumerate(batch):
            read_el = el.numpy()[0]
            if isinstance(ds_info['data'][el_i][0], str):
                assert isinstance(read_el, bytes)
                read_el = read_el.decode()
            # Mark loaded data
            for data_elem_i, data_elem in enumerate(ds_info['data'][el_i]):
                if np.alltrue(data_elem == read_el):
                    data_marks[el_i][data_elem_i] = True
        count += 1

    # Read data must coverage all data
    assert np.all(data_marks)


def test_close_on_delete(hdf5dataset):
    """Test dataset closed on delete."""
    ds_info = hdf5dataset
    dtypes = [el.dtype for el in ds_info['data']]

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ds_info['keys'], dtypes, shuffle=False)

    ds_file = h5_wrapper.ds
    h5_wrapper.__del__()
    # Probably not the best way to test the file is closed: the property is
    # not documented, but public.
    # An alternative is to use MonkeyPatch to mock h5py.File.
    assert not ds_file.id.valid


def test_large_buf_warning(hdf5dataset):
    """Test warning is emitted."""
    ds_info = hdf5dataset
    dtypes = [el.dtype for el in ds_info['data']]

    with pytest.warns(UserWarning, match='buffer_size if bigger than data'):
        cvl.hdf5.tf.TFDatasetWrapper(
            ds_info['path'], ds_info['keys'], dtypes,
            shuffle=False, buffer_size=10
        )


def test_finite_ds(hdf5dataset):
    """Test finite dataset."""
    ds_info = hdf5dataset
    dtypes = [el.dtype for el in ds_info['data']]

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ds_info['keys'], dtypes, shuffle=True)

    ds_iter = iter(h5_wrapper.dataset)
    for i in range(len(ds_info['data'][0])):
        next(ds_iter)

    with pytest.raises(StopIteration):
        next(ds_iter)


def test_repeat_ds(hdf5dataset):
    """Test repeat dataset."""
    ds_info = hdf5dataset
    dtypes = [el.dtype for el in ds_info['data']]

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ds_info['keys'], dtypes, shuffle=True, repeat=2)

    ds_iter = iter(h5_wrapper.dataset)
    for i in range(len(ds_info['data'][0]) * 2):
        next(ds_iter)

    with pytest.raises(StopIteration):
        next(ds_iter)


def test_infinite_ds(hdf5dataset):
    """Test infinite dataset."""
    ds_info = hdf5dataset
    dtypes = [el.dtype for el in ds_info['data']]

    h5_wrapper = cvl.hdf5.tf.TFDatasetWrapper(
        ds_info['path'], ds_info['keys'], dtypes, shuffle=True, repeat=True)

    count = 0
    ds_iter = iter(h5_wrapper.dataset)
    while count < len(ds_info['data'][0]) * 2:
        next(ds_iter)
        count += 1
