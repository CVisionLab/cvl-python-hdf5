"""Packed storage tests."""

import os
import h5py
import numpy as np

import pytest

from cvl import hdf5


def test_packed_ds_shape(tmpdir):
    """Test packed dataset creation."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        data_even=hdf5.DatasetDescription((10, 16), 'bool', pack_axis=-1),
        data_odd=hdf5.DatasetDescription((10, 20), 'bool', pack_axis=-1)
    )
    dsfile.close()
    dsfile = h5py.File(dsfile_name, 'r')

    assert(dsfile['data_even'].shape == (0, 10, 2))
    assert(dsfile['data_odd'].shape == (0, 10, 3))


def test_packed_ds_reading(tmpdir):
    """Test packed dataset reading."""
    # random boolean array
    test_data = np.random.uniform(size=(10, 10, 16)) > 0.5

    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        data=hdf5.DatasetDescription(
            (10, 16), 'bool', pack_axis=-1, attrs={'attr': 10}),
    )
    hdf5.store_data(dsfile, data=test_data)
    dsfile.close()

    dsfile = h5py.File(dsfile_name, 'r')
    ds = hdf5.PackedDS(dsfile, 'data')

    assert(ds.shape == test_data.shape)
    assert(np.alltrue(np.asarray(ds) == test_data))
    assert(ds.attrs['attr'] == 10)


def test_packed_ds_existing_incompatible(tmpdir):
    """Test handling of existing not packed DS"""
    test_data = np.random.uniform(size=(10, 10, 16)) > 0.5

    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        data=hdf5.DatasetDescription(
            (10, 16), 'bool'),
    )
    hdf5.store_data(dsfile, data=test_data)
    dsfile.close()

    dsfile = h5py.File(dsfile_name, 'r')

    with pytest.warns(
            RuntimeWarning,
            match='Dataset already exists and appears to be not packed'):
        ds = hdf5.PackedDS(dsfile, 'data')
    assert(type(ds) == h5py.Dataset)


def test_packed_ds_shape_required(tmpdir):
    """Test shape is required when adding new dataset"""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = h5py.File(dsfile_name, 'w')
    with pytest.raises(RuntimeError):
        hdf5.PackedDS(dsfile, 'dataset')


def test_packed_ds_type_requirements(tmpdir):
    """Test type requirements of existing dataset"""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        data=hdf5.DatasetDescription(
            (10, 16), 'bool',
            attrs={'_packed_ds_src_len': 0, '_packed_ds_axis': 0}
        ),
    )
    with pytest.raises(RuntimeError):
        hdf5.PackedDS(dsfile, 'data')

    dsfile.close()


def test_packed_ds_shape_requirements(tmpdir):
    """Test shape requirements of existing dataset"""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        data=hdf5.DatasetDescription(
            (10, 2), 'uint8',
            attrs={'_packed_ds_src_len': 0, '_packed_ds_axis': -1}
        ),
    )
    with pytest.raises(RuntimeError):
        hdf5.PackedDS(dsfile, 'data', shape=(10, 2))

    hdf5.PackedDS(dsfile, 'data', shape=(10, 16))

    dsfile.close()


def test_packed_ds_basics(tmpdir):
    """Test basic operation: item getter, len."""
    test_data = np.random.uniform(size=(10, 10, 16)) > 0.5

    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = hdf5.init_storage(
        dsfile_name,
        data=hdf5.DatasetDescription(
            (10, 16), 'bool', pack_axis=-1),
    )
    hdf5.store_data(dsfile, data=test_data)
    dsfile.close()

    dsfile = h5py.File(dsfile_name, 'r')
    ds = hdf5.PackedDS(dsfile, 'data')
    assert(np.alltrue(ds[0] == test_data[0]))
    assert(np.alltrue(ds[0:2] == test_data[0:2]))
    assert(np.alltrue(ds[-1] == test_data[-1]))
    assert(np.alltrue(ds[::2] == test_data[::2]))
    assert(len(ds) == len(test_data))


def test_packed_ds_writing(tmpdir):
    """Test packed ds direct creation."""
    dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
    dsfile = h5py.File(dsfile_name, 'w')
    hdf5.PackedDS(dsfile, 'dataset', shape=(10, 10, 16), chunks=None)
