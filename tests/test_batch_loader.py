"""Batch loader tests."""

import os
import h5py
import numpy as np

from pkg_resources import parse_version as vparse

from memory_profiler import memory_usage

import pytest

from cvl import hdf5


class TestLoad:
    """Test class with functions for testing data loading."""

    @pytest.mark.parametrize(
        'string_data',
        [
            pytest.param(
                ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'z'],
                marks=pytest.mark.skipif(
                    vparse(h5py.__version__) >= vparse('3'),
                    reason='h5py uses binary strings starting from 3.0.0'
                )
            ),
            pytest.param(
                [b'a', b'b', b'c', b'd', b'e', b'f', b'g', b'h', b'z'],
                marks=pytest.mark.skipif(
                    vparse(h5py.__version__) < vparse('3'),
                    reason='h5py uses normal strings before 3.0.0'
                )
            )
        ],
        ids=('str', 'bytes')
    )
    def test_general_load(self, tmpdir, string_data):
        """Test batch loader loads all data."""
        ds_names = [
            'data1', 'data2', 'data3'
        ]
        ds_descrs = [
            hdf5.DatasetDescription(tuple(), 'uint8'),
            hdf5.DatasetDescription(tuple(), h5py.special_dtype(vlen='int64')),
            hdf5.DatasetDescription(tuple(), h5py.special_dtype(vlen=str))
        ]
        ds_data = [
            np.arange(1, 10, dtype='uint8'),
            np.array([[1, 2], [3], [5, 7], [2, 1], [1],
                      [25, 30], [20], [100], [0, -1, 4]]),
            string_data
        ]

        dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
        dsfile = hdf5.init_storage(
            dsfile_name, **dict(zip(ds_names, ds_descrs)))
        hdf5.store_data(dsfile, **dict(zip(ds_names, ds_data)))
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')
        loader = hdf5.BatchLoader(2, dsfile)
        read_data = [[] for _ in range(len(ds_names))]
        for i in range(5):
            loaded_data = loader.load(i)
            for k in range(len(read_data)):
                read_data[k].extend(loaded_data[k])
        dsfile.close()

        for k in range(len(ds_names)):
            for j in range(10):
                assert(np.alltrue(read_data[k][j] == ds_data[k][j % 9]))

    def test_batch_count(self, tmpdir):
        """Test batch loader loads all data."""
        test_data = np.arange(1, 10, dtype='uint8')

        dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
        dsfile = hdf5.init_storage(
            dsfile_name,
            data=hdf5.DatasetDescription(tuple(), 'uint8')
        )
        hdf5.store_data(dsfile, data=test_data)
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')

        loader = hdf5.BatchLoader(2, dsfile)
        assert(loader.batch_count() == 5)

        loader = hdf5.BatchLoader(3, dsfile)
        assert(loader.batch_count() == 3)

        with pytest.raises(RuntimeError):
            loader.batch_count(key='missing')

        loader = hdf5.BatchLoader(
            4, None,
            keys={
                'name': hdf5.DatasetDescription(tuple(), 'uint8')
            }
        )
        assert(loader.batch_count() is None)

        dsfile.close()

    def test_indices(self, tmpdir):
        """Test batch loader loads all data."""
        test_data = np.arange(1, 10, dtype='uint8')

        dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
        dsfile = hdf5.init_storage(
            dsfile_name,
            data=hdf5.DatasetDescription(tuple(), 'uint8')
        )
        hdf5.store_data(dsfile, data=test_data)
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')

        loader = hdf5.BatchLoader(4, dsfile)

        indices = set()
        for i in range(loader.batch_count()):
            indices = indices.union(loader.indices(i, 1))
            assert(
                np.alltrue(loader.load(i, 1) ==
                           test_data[loader.indices(i, 1)]))
        assert(list(indices) == np.arange(9).tolist())

        with pytest.raises(RuntimeError):
            loader.indices(0, 1, key='missing')

        loader = hdf5.BatchLoader(
            4, None,
            keys={
                'name': hdf5.DatasetDescription(tuple(), 'uint8')
            }
        )
        assert(loader.indices(0, 1) is None)

        dsfile.close()


class TestFilelessUsage:
    """Test class with functions for testing fileless initialized loader."""

    def test_init(self):
        """Test batch loader initialization without file."""
        hdf5.BatchLoader(
            4, None,
            keys={
                'name': hdf5.DatasetDescription(tuple(), 'uint8')
            }
        )

    def test_usage_after_init(self):
        """
        Test batch loader requires file to call load if initialized fileless.
        """
        loader = hdf5.BatchLoader(
            4, None,
            keys={
                'name': hdf5.DatasetDescription(tuple(), 'uint8')
            }
        )
        with pytest.raises(ValueError):
            loader.load(0)


class TestArgs:
    """Test class with functions for testing args usage and storing."""

    def test_batch_loader_requires_file_or_keys(self, tmpdir):
        """Test batch loader requirements for datafile or keys."""
        with pytest.raises(ValueError):
            hdf5.BatchLoader(5, None)

        with pytest.raises(TypeError):
            hdf5.BatchLoader(5, None, keys=[])

        with pytest.raises(TypeError):
            hdf5.BatchLoader(5, None, keys={'name': 'Wrong type'})

    def test_wrong_file(self, tmpdir):
        """Test passing data file as string (path)."""
        with pytest.raises(IOError):
            hdf5.BatchLoader(5, 'wrong-file-name')

    def test_internal_fields(self, tmpdir):
        """Test datafile attribute."""
        test_data = np.random.uniform(size=(10, 10, 16)) > 0.5

        dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
        dsfile = hdf5.init_storage(
            dsfile_name,
            data=hdf5.DatasetDescription(
                (10, 16), 'bool'),
        )
        hdf5.store_data(dsfile, data=test_data)
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')
        loader = hdf5.BatchLoader(5, dsfile)
        assert(dsfile is loader.datafile)
        assert(loader.keys == ['data'])
        assert(loader.batch_size == 5)
        dsfile.close()

        loader = hdf5.BatchLoader(5, dsfile_name)
        assert(type(loader.datafile) == h5py.File)
        assert(loader.datafile.filename == dsfile_name)


class TestPacked:
    """Test class with functions for testing PackedDS handling."""

    def test_load(self, tmpdir):
        """Test packed dataset reading."""
        # random boolean array
        test_data = np.random.uniform(size=(10, 10, 16)) > 0.5

        dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
        dsfile = hdf5.init_storage(
            dsfile_name,
            data=hdf5.DatasetDescription(
                (10, 16), 'bool', pack_axis=-1),
        )
        hdf5.store_data(dsfile, data=test_data)
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')
        loader = hdf5.BatchLoader(5, dsfile)

        assert(np.alltrue(np.asarray(loader.load(0)) == test_data[:5]))
        assert(np.alltrue(np.asarray(loader.load(1)) == test_data[5:]))

        dsfile.close()

    def test_false_packed_load(self, tmpdir):
        """
        Test batch loader fails when axis said to be packed but in fact not.
        """
        # random boolean array
        test_data = np.random.uniform(size=(10, 10, 16)) > 0.5

        dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
        dsfile = hdf5.init_storage(
            dsfile_name,
            data=hdf5.DatasetDescription(
                (10, 16), 'bool'),
        )
        hdf5.store_data(dsfile, data=test_data)
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')
        loader = hdf5.BatchLoader(
            5, None,
            keys={
                'data': hdf5.DatasetDescription((10, 16), 'bool', pack_axis=-1)
            }
        )
        with pytest.raises(KeyError):
            loader.load(0, datafile=dsfile)
        dsfile.close()


class TestMemoryLeaks:
    """
    Test possible memory leaks in data reading with BatchLoader.
    """

    @staticmethod
    def read_batches(loader, num_reads):
        """
        Read random batches from the given ``loader`` ``num_reads`` times.

        Parameters
        ----------
        loader : hdf5.dataset.BatchLoader
            Initialized `BatchLoader`.
        num_reads : int
            Number of random batch reads from the given batch loader.

        """
        for _ in range(num_reads):
            loader.load(
                np.random.randint(0, loader.batch_count()),
                np.random.randint(0, loader.batch_size))

    def test_generic_ds(self, tmpdir):
        """
        Test reading from simple HDF5 dataset (not packed, fixed length).
        """
        # random boolean array
        test_data = np.random.uniform(size=(10, 10, 16)) > 0.5

        dsfile_name = os.path.join(str(tmpdir), 'data.hdf5')
        dsfile = hdf5.init_storage(
            dsfile_name,
            data=hdf5.DatasetDescription(
                (10, 16), 'bool'),
        )
        hdf5.store_data(dsfile, data=test_data)
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')
        loader = hdf5.BatchLoader(5, dsfile)

        profile = memory_usage(
            (TestMemoryLeaks.read_batches, (loader, 10000), {})
        )
        dsfile.close()
        p1_median = np.median(profile[:len(profile) // 2])
        p2_median = np.median(profile[len(profile) // 2:])
        mem_diff = p2_median - p1_median
        # Not more than 1Mb of memory, chosen empirically.
        assert mem_diff < 1

    def test_varlen_ds(self, tmpdir):
        """
        Test reading from dataset containing variable length sequences.
        """
        # random boolean array
        ds_descr = hdf5.DatasetDescription(tuple(),
                                           h5py.special_dtype(vlen='int64'))
        ds_data = np.array([[1, 2], [3], [5, 7], [2, 1], [1],
                            [25, 30], [20], [100], [0, -1, 4]])

        dsfile_name = tmpdir / 'data.hdf5'
        dsfile = hdf5.init_storage(dsfile_name, data=ds_descr)
        hdf5.store_data(dsfile, data=ds_data)
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')
        loader = hdf5.BatchLoader(5, dsfile)

        profile = memory_usage(
            (TestMemoryLeaks.read_batches, (loader, 10000), {})
        )
        dsfile.close()
        p1_median = np.median(profile[:len(profile) // 2])
        p2_median = np.median(profile[len(profile) // 2:])
        mem_diff = p2_median - p1_median
        # Not more than 1Mb of memory, chosen empirically.
        assert mem_diff < 1

    def test_packed_ds(self, tmpdir):
        """
        Test reading from packed dataset.
        """
        # random boolean array
        test_data = np.random.uniform(size=(10, 10, 16)) > 0.5

        dsfile_name = tmpdir / 'data.hdf5'
        dsfile = hdf5.init_storage(
            dsfile_name,
            data=hdf5.DatasetDescription(
                (10, 16), 'bool', pack_axis=-1),
        )
        hdf5.store_data(dsfile, data=test_data)
        dsfile.close()

        dsfile = h5py.File(dsfile_name, 'r')
        loader = hdf5.BatchLoader(5, dsfile)

        profile = memory_usage(
            (TestMemoryLeaks.read_batches, (loader, 10000), {})
        )
        dsfile.close()
        p1_median = np.median(profile[:len(profile) // 2])
        p2_median = np.median(profile[len(profile) // 2:])
        mem_diff = p2_median - p1_median
        # Not more than 1Mb of memory, chosen empirically.
        assert mem_diff < 1
