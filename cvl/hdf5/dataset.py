"""Dataset initialization and data writing."""

import h5py
import numpy as np

from collections import namedtuple

from .packed import PackedDS


class DatasetDescription(namedtuple(
        'DatasetDescription',
        ['shape', 'dtype', 'attrs',
         'chunksize', 'compression', 'compression_opts',
         'pack_axis'])):
    """
    DatasetDescription describes HDF5 dataset.

    Attributes are read-only and can only be set at construction.

    Attributes
    ----------
        shape: tuple
            Dataset partial shape (without first axis)
        dtype: h5py.dtype
            Dataset data type
        attrs: Mapping, optional
            Mapping of dataset attributes (defaults to no attrs)
        chunksize: int, optional
            HDF5 dataset chunk size (default is unset).
        compression: str, optional
            HDF5 dataset compression mode.
            Default use ``h5py`` default behavior.
        compression_opts: int, optional
            Sets the compression level and may be an integer from 0 to 9,
            default is 4. Ignores if `compression` is ``None``.
        pack_axis: int, optional
            Axis to pack using np.packbits. Default is to use unpacked dataset.
    """

    def __new__(cls, shape, dtype, attrs=None, chunksize=None,
                compression=None, compression_opts=None, pack_axis=None):
        """Construct DatasetDescription instance."""
        args = locals()
        del args['cls']
        args.pop('__class__', None)
        return super(DatasetDescription, cls).__new__(cls, **args)
# The above is a wrapper class around namedtuple for the sake of providing
# custom docstring and defaults


def init_storage(fname, chunksize=100, attrs=None, file_mode='w',
                 **kwargs):
    """
    Init resizable dataset file.

    Parameters
    ----------
    fname : str
        storage file name
    chunksize : int, optional
        desired chunk size (True for auto size)
    attrs : dict, optional
        storage attributes mapping
    file_mode : string, optional
        HDF5 file open mode (see h5py.File documentation).
    **kwargs : DatasetDescription
        datasets definitions (parameter name is a dataset name, value has to
        be a DatasetDescription object)

    Raises
    ------
    TypeError
        If argument passed via kwargs is not of
        DatasetDescription type

    Returns
    -------
    HDF5 File object with initialized datasets
    """
    df = h5py.File(fname, file_mode)
    if attrs is not None:
        for aname, avalue in attrs.items():
            df.attrs[aname] = avalue
    df._pds_wrappers = {}
    for dsname, dsinfo in kwargs.items():
        if not isinstance(dsinfo, DatasetDescription):
            raise TypeError(
                ('Dataset definition has to be a DatasetDescription object. '
                 'Passed argument "{name}" has type {type}').format(
                    name=dsname, type=type(dsinfo))
            )
        chunks = chunksize if dsinfo.chunksize is None else dsinfo.chunksize
        if isinstance(chunks, int):
            chunks = (chunks,) + dsinfo.shape

        if dsinfo.pack_axis is not None:
            ds_wrapper = PackedDS(df, dsname,
                                  shape=(0,) + dsinfo.shape,
                                  maxshape=(None,) + dsinfo.shape,
                                  dtype=dsinfo.dtype,
                                  chunks=chunks,
                                  compression=dsinfo.compression,
                                  compression_opts=dsinfo.compression_opts)
            df._pds_wrappers[dsname] = ds_wrapper
        else:
            df.create_dataset(dsname,
                              shape=(0,) + dsinfo.shape,
                              maxshape=(None,) + dsinfo.shape,
                              dtype=dsinfo.dtype,
                              chunks=chunks,
                              compression=dsinfo.compression,
                              compression_opts=dsinfo.compression_opts)
        if dsinfo.attrs is not None:
            for aname, avalue in dsinfo.attrs.items():
                df[dsname].attrs[aname] = avalue
    return df


def store_data(storage, **kwargs):
    """
    Write data to a resizable HDF5 storage.

    Parameters
    ----------
    storage : h5py.File | h5py.Group
        HDF5 file File or Group object
    **kwargs : Sequence
        Data to add to datasets. Parameter names should match to dataset names.
        dataset value can be a sequence of values of just one value

    Raises
    ------
    KeyError
        Storage doesn't contain dataset with passed name.
    """
    for dsname, value in kwargs.items():
        if dsname not in storage:
            raise KeyError('There is no {0} dataset in the storage'.format(
                dsname))
        ds = storage[dsname]
        elem_shape = ds.shape[1:]
        value_shape = np.shape(value)
        if elem_shape == value_shape:
            value = [value]
        ds.resize(ds.shape[0] + len(value), axis=0)
        ds = storage._pds_wrappers.get(dsname, ds)
        ds[-len(value):] = value
