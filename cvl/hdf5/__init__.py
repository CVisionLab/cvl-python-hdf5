"""
HDF5 utilities to simplify working with HDF5.

1. HDF5 data files creation (see `hdf5.dataset`).
2. Efficient HDF5 loading (see `hdf5.loader`).
3. Bit-packed datasets for efficient storage of binary (masks) data (see
   `hdf5.packed`).
4. TensorFlow Dataset based on HDF5-based Dataset (see `hdf5.tf`).
   This module is not pulled by default when importing `hdf5` package as it
   depends on ``tensorflow`` package.
"""
from .version import __version__  # noqa: ignore unused import
from .dataset import DatasetDescription, init_storage, store_data  # noqa
from .loader import BatchLoader  # noqa
from .packed import PackedDS  # noqa
