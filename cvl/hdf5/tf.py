"""General wrapper for tf.Dataset that loads hdf5 data."""

import h5py
import tensorflow as tf
import warnings

from .loader import BatchLoader


class TFDatasetWrapper(object):
    """
    Class-wrapper for loading HDF5 file in TF pipeline.

    Assumptions
    ----------
    * dataset contain attribute with name ``size`` = count of all samples that
      would be read.
    * ``outside_graph_fn`` signature is:

      .. code-block:: python

        def out_f(k1_data, k2_data, ..., kn_data):
            return p1_data, p2_data, ..., pl_data

      ``dtypes`` specify types of ``p1_data, p2_data, ..., pl_data``

      ``k1_data, k2_data, ..., kn_data`` - loaded arrays from h5-file.

      ``inside_graph_fn`` signature then is:

      .. code-block:: python

        def in_f(p1_data, p2_data, ..., pl_data):
            return data1, ..., datam

      and if there is no ``outside_graph_fn``, ``k1_data, ..., kn_data``
      are used as args for ``inside_graph_fn``.

    Limitations
    ----------
    * Loaded ``h5py.Dataset`` arrays should have the same size, which
      is specified in attributes (look **Assumptions** block)

    Recommendations
    ----------
    * ``buffer_size`` selection advices:
        1. Each sample processing is heavy -> ``buffer_size < batch_size``
        2. Each sample processing is light -> ``buffer_size > batch_size``
           but is must be less then ``size`` of the dataset
           (limitation of ``cvl.hdf5.BatchLoader``)
    * It is faster to use ``repeat=True`` than use fixed dataset.

    """

    def __del__(self):
        """Close file when wrapper has been destroyed."""
        self.ds.close()

    def __init__(self, ds_path, keys, dtypes,
                 inside_graph_fn=None, outside_graph_fn=None,
                 shuffle=False, buffer_size=1, batch_size=1,
                 nthreads=None, prefetch_size=1, repeat=False):
        """
        Init instance, create ``tf.data.Dataset`` object.

        Parameters
        ----------
        ds_path : str
            Path to the hdf5 file.
        keys : Sequence
            Names from hdf5 to load.
        dtypes : Sequence
            Types of the data that is returned from data loader
            or from outside graph function if it is specified.
        inside_graph_fn : function
            Function for preprocessing TF-placeholders.
        outside_graph_fn : function
            Function for preprocessing raw read data.
        shuffle : bool
            Shuffle data flag.
        buffer_size : int
            How many examples load each thread
        batch_size : int
            How many examples to add in batch.
            if ``None`` then use no batching.
            It can be useful if you want to do some extra processing
        nthreads : int
            How many threads load data in one time.
            if ``None``, then auto-select.
        prefetch_size : int
            Prefetching allows to speed-up train process.
            This value define how many extra load iteration to do.
            For example, default value is 1.
            It means, that after N iteration is ready, CPU calculates N+1
            iteration.
            If ``None`` then use no prefetch.
            It can be useful if you want to do some extra processing.
        repeat: bool or int
            If ``True`` then infinite repeat dataset, else do not.
            If ``int`` then repeat dataset specified times count.


        Raises
        ----------
        RuntimeError
            If passed value for ``repeat`` is undefined.
        """
        self.buffer_size = buffer_size
        self.prefetch_size = prefetch_size
        self.keys = keys
        self.dtypes = dtypes
        self.ds = h5py.File(ds_path, 'r')
        self.data_size = self.ds.attrs['size']
        self.batch_size = batch_size

        if self.buffer_size > self.data_size:
            warnings.warn('buffer_size if bigger than data.')

        def next_buf(idx):  # pragma: no cover (invoked from TF)
            loader = BatchLoader(self.buffer_size, self.ds, self.keys)
            data = loader.load(idx, datafile=self.ds)
            #
            if outside_graph_fn is not None:
                proc_data = outside_graph_fn(*data)
            else:
                proc_data = data
            return proc_data

        def out_fn_wrap(*args):  # pragma: no cover (invoked from TF)
            return tf.py_function(next_buf, args, dtypes, name='batch_loader')

        with tf.device('/cpu:0'):
            dataset = tf.data.Dataset.range(
                0, self.data_size, self.buffer_size)
            if shuffle:
                dataset = dataset.shuffle(self.data_size)

            if isinstance(repeat, bool):
                if repeat:
                    dataset = dataset.repeat()
            elif isinstance(repeat, int):
                dataset = dataset.repeat(repeat)
            else:
                raise RuntimeError(
                    'It is allowed for `repeat` to be `bool` or `int` only.')

            dataset = dataset.map(out_fn_wrap, nthreads)
            if inside_graph_fn is not None:
                dataset = dataset.map(inside_graph_fn, nthreads)

            self.dataset = dataset.unbatch()
            if self.batch_size is not None:
                self.dataset = self.dataset.batch(batch_size)
            if self.prefetch_size is not None:
                self.dataset = self.dataset.prefetch(prefetch_size)
