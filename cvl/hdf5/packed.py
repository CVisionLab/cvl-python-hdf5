"""Packed storage in h5 dataset implementation."""

import warnings
import numpy as np


class PackedDS(object):
    """
    Transparent wrapper around h5py datasets for packed boolean data storage.

    Wrapper can create new packed dataset or use existing. In the later case
    wrapper fallbacks to h5py dataset if existing dataset doesn't look as
    a packed dataset (and throws runtime warning).

    Limitations
    -----------
    * Only one axis can be packed.
    * Class doesn't check if data is really boolean. In case of using integer
      base data type it's a user's responsibility to use only lowest bit
      data from other bits will be lost.
    * Class explicitly supports handling of ``chunks`` and ``maxshape``
      arguments at dataset creation. All other arguments if they need shape
      transformation are not supported.
    """

    def __new__(cls, hdfgroup, dsname, **kwargs):
        """
        Constructor.

        In case of existing dataset checks if it packed and fallbacks to
        original h5py dataset object otherwise.

        May return either PackedDS object or ``h5py.Dataset``

        Parameters
        ----------
        hdfgroup : h5py.Group
            HDF5 group of the dsname dataset (``h5py.File`` also implements
            ``h5py.Group`` API).
        dsname : str
            Dataset name.
        **kwargs
            Additional arguments (used when creating dataset).

        Returns
        -------
        PackedDS | h5py.Dataset
            Either created PackedDS object or plain h5py.Dataset object.
        """
        # handle existing dataset of incompatible type by replacing
        # class with original dataset
        if dsname in hdfgroup:
            if ('_packed_ds_src_len' not in hdfgroup[dsname].attrs or
                    '_packed_ds_axis' not in hdfgroup[dsname].attrs):
                warnings.warn(
                    'Dataset already exists and appears to be not packed',
                    RuntimeWarning
                )
                return hdfgroup[dsname]
        return super(PackedDS, cls).__new__(cls)

    def __init__(self, hdfgroup, dsname, shape=None, axis=-1, **kwargs):
        """
        Perform initialization of PackedDS object.

        Parameters
        ----------
        hdfgroup : h5py.Group
            HDF5 group of the dsname dataset (h5py.File also implements
            h5py.Group API).
        dsname : str
            Dataset name.
        shape : tuple, optional
            Shape of the dataset.
            Required if dataset doesn't exist in the group. If dataset exists
            and shape is not None, dataset shape will be checked against passed
            value.
        axis : int, optional
            Axis to pack. Default is to pack the last axis.
        **kwargs
            Additional arguments passed to ``create_dataset``

        Raises
        ------
        RuntimeError
            existing dataset has incompatible shape
        """
        self._group = hdfgroup
        # check if dataset need to be created
        if dsname not in self._group:
            # missing shape argument -> not enough information to create DS
            if shape is None:
                raise RuntimeError(
                    "Shape argument is required if dataset doesn\'t exist")
            self._axis = axis
            self._src_len = shape[self._axis]
            self._packed_len = -(-self._src_len // 8)
            if 'dtype' in kwargs:
                del kwargs['dtype']
            kwargs['shape'] = shape
            self._fix_shape_kwargs(kwargs, ['shape', 'chunks', 'maxshape'])
            self._group.create_dataset(
                dsname, dtype=np.uint8, **kwargs)
            self._ds = self._group[dsname]
            self._ds.attrs['_packed_ds_src_len'] = self._src_len
            self._ds.attrs['_packed_ds_axis'] = self._axis
        else:
            self._ds = self._group[dsname]
            self._src_len = self._ds.attrs['_packed_ds_src_len']
            self._axis = self._ds.attrs['_packed_ds_axis']

            # check data type
            if self._ds.dtype != np.uint8:
                raise RuntimeError('Existing dataset has incompatible type')

            # if shape is passed check compatibility
            if shape is not None:
                shape = list(shape)
                packed_len = -(-shape[self._axis] // 8)
                shape[self._axis] = packed_len
                if self._ds.shape[1:] != tuple(shape):
                    raise RuntimeError(
                        'Existing dataset has incompatible shape: {0} vs {1} '
                        '(computed)'.format(self._ds.shape, shape))

    @property
    def shape(self):
        """
        Dataset shape information (unpacked).

        Returns
        -------
        tuple
            Dataset shape.
        """
        shape = list(self._ds.shape)
        shape[self._axis] = self._src_len
        return tuple(shape)

    @property
    def attrs(self):
        """
        Dataset attributes.

        Returns
        -------
        h5py.AttributeManager
            Dataset attributes.
        """
        return self._ds.attrs

    def __getitem__(self, key):
        """
        Item getter implementation.
        """
        return np.unpackbits(
            self._ds[key], axis=self._axis)[..., :self._src_len]

    def __setitem__(self, key, value):
        """
        Item setter implementation
        """
        self._ds[key] = np.packbits(value, axis=self._axis)

    def __len__(self):
        """
        Dataset length.
        """
        return len(self._ds)

    def __array__(self):
        """
        Dataset to array convertion.
        """
        return np.unpackbits(self._ds.__array__(), axis=self._axis)[
            ..., :self._src_len]

    def _fix_shape_kwargs(self, kwargs, argnames):
        """
        Replace shapes in the passed argnames with their packed equivalent.

        Parameters
        ----------
        kwargs : dict
            kwargs dictionary.
        argnames : list
            List of argnames that are subject for shape transformation.
        """
        for argname in argnames:
            if argname not in kwargs or kwargs[argname] is None:
                continue
            shape_as_list = list(kwargs[argname])
            shape_as_list[self._axis] = self._packed_len
            kwargs[argname] = tuple(shape_as_list)
