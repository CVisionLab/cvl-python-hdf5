"""Batch loader class implementation."""

import h5py
import numpy as np

from collections.abc import Mapping

from .dataset import DatasetDescription


class BatchLoader(object):
    """
    Efficiently load batch data using read_direct and sequential read.

    BatchLoader can be constructed with or without datafile. In the later
    case it should be provided with a mapping of keys to DatasetDescription
    objects with defined shape and dtype of the datasets that will be read
    by batch loader.

    If BatchLoader constructed on an existing datafile then keys argument may
    be omitted. In this case the list of keys will be obtained from datafile.
    One can specify list of keys in order to restrict loading to a specific
    datasets (more memory efficient) or to set required order of data in output
    of `load` call.

    If BatchLoader constructed with existing datafile it still can be used with
    another (opened) datafile if corresponding argument passed to the `load`
    call.

    BatchLoader returns readonly views of internal arrays from its `load` call.
    This design decision (together with fixed batch size) allows reuse of
    allocated internal arrays and avoids their reallocation at every call to
    `load`.
    """

    def __init__(self, batch_size, datafile, keys=None):
        """
        Construct BatchLoader object.

        Parameters
        ----------
        batch_size : int
            Batch size.
        datafile : h5py.File | str
            Opened h5py.File object or file name (will be opened in readonly
            mode).
        keys : Iterable | Mapping
            Iterable of dataset names that this BatchLoader should load.
            If datafile is None then this argument becomes required and should
            be a mapping from dataset names to DatasetDescription objects.
            In case of packed dataset pack_axis should be set to True.

        Raises
        ------
        TypeError
            If datafile is omitted keys should be a mapping from dataset names
            to DatasetDescription objects.
        ValueError
            Either datafile or keys should be defined.
        """
        self._arrays = {}  # data arrays used for reading and writing
        self._views = {}   # readonly views of data arrays
        self._packed = {}  # flags meaning that dataset is packed
        self._batch_size = batch_size
        # if DS will be opened inside, then close inside too
        self._need_to_close = False

        if datafile is None and keys is None:
            raise ValueError('Either datafile or keys should be defined')
        if datafile is None:
            if not isinstance(keys, Mapping):
                raise TypeError('If datafile is omitted keys should be a '
                                'mapping from dataset names to '
                                'DatasetDescription objects')
            if not np.all(
                    list(map(isinstance, keys.values(),
                             [DatasetDescription] * len(keys)))
            ):
                raise TypeError('If datafile is omitted keys should be a '
                                'mapping from dataset names to '
                                'DatasetDescription objects')
            # build arrays based on shapes and dtypes
            for key, dsd in keys.items():
                self._arrays[key] = np.empty((batch_size,) + dsd.shape,
                                             dsd.dtype)
                self._packed[key] = dsd.pack_axis is not None
            self._keys = list(keys.keys())
        else:
            # open file if string passed
            if not isinstance(datafile, h5py.File):
                datafile = h5py.File(datafile, 'r')
                self._need_to_close = True
            if keys is None:
                keys = datafile.keys()
            self._keys = list(keys)
            # obtain shapes from dataset and build
            for key in keys:
                self._arrays[key] = np.empty(
                    (batch_size,) + datafile[key].shape[1:],
                    datafile[key].dtype)
                if ('_packed_ds_src_len' in datafile[key].attrs and
                        '_packed_ds_axis' in datafile[key].attrs):
                    self._packed[key] = (
                        datafile[key].attrs['_packed_ds_axis'],
                        datafile[key].attrs['_packed_ds_src_len']
                    )
                else:
                    self._packed[key] = False

        for name, arr in self._arrays.items():
            view = arr.view()
            view.flags.writeable = False
            self._views[name] = view

        self._datafile = datafile

    def __del__(self):
        """Run on deletion."""
        if self._need_to_close:
            self.datafile.close()

    def load(self, index, offset=0, keys=None, datafile=None):
        """
        Load batch data using batch index and batch offset.

        Using random index and offset one can obtain random batch of
        sequential data (sequential read is much faster).

        A good practice is to generate random offset at each training epoch and
        use randomly permuted indexes to load batches at epoch steps.

        Parameters
        ----------
        index : int
            Index of a batch to load (0-based).
        offset : int, optional
            Optional offset from the beginning of the dataset 0..batch_size-1.
        keys : Iterable, optional
            Dataset names to load (should be a subset of the keys specified at
            construction time and datafile keys).
            Keys argument also defines order of data in the returned tuple.
        datafile : h5py.File, optional
            Datafile to load data from. Can be omitted if was specified at
            construction time otherwise, required.
            If passed should be compatible (shapes and dtypes) with one passed
            at construction time or with definitions passed at construction
            time.

        Returns
        -------
        tuple
            Tuple of values read from the data file in keys order.

        Raises
        ------
        ValueError
            Datafile was not specified at construction time and was not passed.
        """
        keys = keys or self._keys
        datafile = datafile or self._datafile
        if datafile is None:
            raise ValueError('Datafile was not specified at construction time '
                             'and should be explicitly passed to load')
        size = self._batch_size
        read_values = []
        for key in keys:
            dataset = datafile[key]
            datalen = len(dataset)
            start = (offset + size * index) % datalen
            end = (start + size)
            if end > datalen:
                end = end % datalen
            output = self._arrays[key]

            # if dataset elem len is variable,
            # then don't read directly because of mem leaks
            read_direct = dataset.dtype.kind != 'O'

            # Build selectors for reading and writing values
            range_selectors = []
            if end > start:
                range_selectors.append((np.s_[start:end], np.s_[:]))
            else:
                lastsize = datalen - start
                range_selectors.append((np.s_[start:], np.s_[:lastsize]))
                range_selectors.append((np.s_[:end], np.s_[lastsize:]))

            # Read values either directly or using slice assingment
            for src, dst in range_selectors:
                if read_direct:
                    dataset.read_direct(output, src, dst)
                else:
                    output[dst] = dataset[src]

            if self._packed[key] is False:
                read_values.append(self._views[key])
                continue
            elif self._packed[key] is True:
                self._packed[key] = (
                    dataset.attrs['_packed_ds_axis'],
                    dataset.attrs['_packed_ds_src_len']
                )
            read_values.append(
                np.unpackbits(
                    self._views[key], axis=self._packed[key][0]
                )[..., :self._packed[key][1]]
            )
        return tuple(read_values)

    @property
    def datafile(self):
        """Return data file passed at construction."""
        return self._datafile

    @property
    def keys(self):
        """Return keys of the loader."""
        return self._keys

    @property
    def batch_size(self):
        """Return batch size of the loader."""
        return self._batch_size

    def batch_count(self, datafile=None, key=None):
        """
        Return number of batches.

        Parameters
        ----------
        datafile : h5py.File, optional
            Opened HDF5 file.
        key : str, optional
            Dataset name.

        Returns
        -------
        int
            Number of batches.

        Raises
        ------
        RuntimeError
            Dataset with name ``key`` is not found in HDF5 file.
        """
        datafile = datafile or self._datafile
        if datafile is None:
            return None
        # use passed key or first key from self._keys, defaults to None
        key = key or (len(self._keys) > 0 and self._keys[0]) or None
        if key not in datafile:
            raise RuntimeError('Key {0} not found in datafile {1}'.format(
                key, datafile.filename
            ))
        # rounding towards positive infinity
        return -(-len(datafile[key]) // self._batch_size)

    def indices(self, index, offset=0, datafile=None, key=None):
        """
        Return indices of samples that will be selected by the `load` call.

        Parameters
        ----------
        index : int
            Batch index.
        offset : int, optional
            Batch offset.
        datafile : h5py.File, optional
            Opened HDF5 file.
        key : str, optional
            HDF5 dataset name.

        Returns
        -------
        list
            List of indices.

        Raises
        ------
        RuntimeError
            Dataset with name ``key`` is not found in HDF5 file.
        """
        datafile = datafile or self._datafile
        if datafile is None:
            return None
        # use passed key or first key from self._keys, defaults to None
        key = key or (len(self._keys) > 0 and self._keys[0]) or None
        if key not in datafile:
            raise RuntimeError('Key {0} not found in datafile {1}'.format(
                key, datafile.filename
            ))
        datalen = len(datafile[key])
        return [
            idx % datalen
            for idx in range(index * self._batch_size + offset,
                             (index + 1) * self._batch_size + offset)
        ]
