# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
Add unreleased changes below. Do not remove this line.

## [v1.0.2] - 2022-08-24
### Added
* Python3.10 support.

[v1.0.2]: https://gitlab.com/CVisionLab/cvl-python-hdf5/-/compare/v1.0.1...v1.0.2


## [v1.0.1] - 2022-05-05
### Added
* Support of h5 default compression modes in DS-description class.
* Simple tests of the new functionality.

### Fixed
* `h5py` has a bug with read-direct in `v3.0.0` & `v3.1.0`, these versions were
removed from support.

### Changed
* Removed coding header because UTF-8 is default for py3.

[v1.0.1]: https://gitlab.com/CVisionLab/cvl-python-hdf5/-/compare/v1.0.0...v1.0.1


## [v1.0.0] - 2021-04-05
### Added
* Documented project compatibility policy. The project will follow officially supported python version as defined in [Python Developer's Guide](https://devguide.python.org/#status-of-python-branches).

### Changed
* Dropped support for python3.4, python3.5 and added support for python3.9.

[v1.0.0]: https://gitlab.com/CVisionLab/cvl-python-hdf5/-/compare/v0.6.1...v1.0.0


## [0.6.1] - 2020-01-23
### Changed
* Changed API of `cvl.hdf5.tf`: make opportunity to create finite and infinite
datasets.
* Added `README.md` into sphinx documentation.

## [0.6.0] - 2019-12-16
### Changed
* Removed python2 support.
* Implemented tf.data.Dataset wrapper for reading h5-files. Implemented tests.
* Implemented CI.
* Fix BatchLoader reading varlen datasets.
