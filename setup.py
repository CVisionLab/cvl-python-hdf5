"""HDF5 helpers (h5py based)."""
from setuptools import setup, find_namespace_packages

setup(
    name='cvl-hdf5',

    use_scm_version=True,

    author='Dmitri Lapin',
    author_email='lapin@cvisionlab.com',
    description=__doc__,

    packages=find_namespace_packages(include=['cvl.*']),

    python_requires='>=3.6',
    setup_requires=['setuptools_scm'],
    install_requires=[
        'h5py!=3.1.0, !=3.0.0',
        'numpy'
    ],
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    zip_safe=False
)
