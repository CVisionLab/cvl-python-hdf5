$(document).ready(function () {
  $.ajax({
    cache: false,
    url: DOCUMENTATION_OPTIONS.URL_ROOT + '../versions.json',
    dataType: "json",
    success: function(versions) {
      var dd_template = $('.rst-versions .rst-other-versions .versions dd').detach();
      dd_template.children('.current').removeClass('current');
      var versions_list = $('.rst-versions .rst-other-versions .versions');

      var current_version = DOCUMENTATION_OPTIONS.VERSION;

      for (const ver of versions) {
        dd_template.children('a')
          .attr('href', ver['url'])
          .text(ver['name'])
          .toggleClass('current', current_version == ver['name']);
        dd_template.clone().appendTo(versions_list);
      }
      dd_template.remove();
    }
  });
})