cvl-hdf5
========

HDF5 helpers to reduce boilerplate when creating and reading HDF5 datasets.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   api/cvl.hdf5


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
